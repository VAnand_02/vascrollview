//
//  UIView+Debug.swift
//  VASymbolicateTest
//
//  Created by Vikash Anand on 25/05/19.
//  Copyright © 2019 Vikash Anand. All rights reserved.
//

import UIKit

@nonobjc extension UIView {
    
    func logSiblingsRecursive() {
        let subViews = self.value(forKey: "recursiveDescription")!
        print("\(subViews)")
    }
}
