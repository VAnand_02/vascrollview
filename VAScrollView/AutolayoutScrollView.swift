//
//  AutolayoutScrollView.swift
//  VASymbolicateTest
//
//  Created by Vikash Anand on 25/05/19.
//  Copyright © 2019 Vikash Anand. All rights reserved.
//

import UIKit

class AutolayoutScrollView: UIView {
    
    private let tVC = UITableViewController()
    private let scrollView = UIScrollView()
    var containerView:UIView?
    
    private lazy var _containerView: UIView = {
        
        if let containerView = self.subviews.first {
            return containerView
        } else if let containerView = self.containerView {
            return containerView
        } else {
            guard let containerView = UIView.loadNib(withName: "DefaultContainerView") else {
                fatalError("Default container view for AutolayoutScrollView not found...")
            }
            return containerView
        }
    }()
    
    @IBOutlet weak var delegate: UIScrollViewDelegate? {
        didSet {
            self.scrollView.delegate = self.delegate
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setup()
    }
    
    init() {
        super.init(frame: .zero)
        self.setup()
    }
    
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init(containerView: UIView) {
        self.containerView = containerView
        super.init(frame: .zero)
        
        self.setup()
    }
    
    required init(containerViewXibName: String) {
        self.containerView = UIView.loadNib(withName: containerViewXibName)
        super.init(frame: .zero)
        
        self.setup()
    }
    
    /*  This method removes the container view from its parent view so that that scrollview can be added as a
     immidiate child of the parent view. After scrollview is added, container view is added back into the hirerchey
     as a child of scrollview. i.e.
     
     Initial Hierarchy
     -----------------
     AutolayoutScrollView
     --ContainerView
     
     Final Hierarchy
     ---------------
     AutolayoutScrollView
     --Scrollview
     ----ContainerView
     */
    private func setup() {
        
        if let superview = self.superview, superview.subviews.count > 1 {
            fatalError("AutolayoutScrollView can have only immidiate child")
        }
        
        self._containerView.backgroundColor = UIColor.green
        if self._containerView.superview != nil {
            //print("Before removing \(self.logSiblingsRecursive())")
            self._containerView.removeFromSuperview()
        }
        
        self.scrollView.backgroundColor = UIColor.red
        self.add(self.scrollView, withInsets: .zero)
        self.scrollView.add(self._containerView, withInsets: .zero)
        
        /*
         Making height and width constraints equal between scrollview and container view with height constraint
         priority equal to 250
        */
        let heightAnchor = self._containerView.heightAnchor.constraint(equalTo: self.scrollView.heightAnchor)
        heightAnchor.priority = UILayoutPriority(250)
        NSLayoutConstraint.activate([
            self._containerView.widthAnchor.constraint(equalTo: self.scrollView.widthAnchor),
            heightAnchor,
            ])
        
        //print("After adding again \(self.logSiblingsRecursive())")
    }
}
