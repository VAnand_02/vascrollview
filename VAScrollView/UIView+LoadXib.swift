//
//  Bundle+LoadXib.swift
//  VAScrollView
//
//  Created by Vikash Anand on 25/05/19.
//  Copyright © 2019 Vikash Anand. All rights reserved.
//

import UIKit

extension UIView {
    
    static func loadNib<T: UIView>(withName xibName: String) -> T? {
        guard let loadedView = Bundle.main.loadNibNamed(xibName,
                                                 owner: self, options: nil)?.first as? T
            else { fatalError("Xib with name:\(xibName) not found...") }
        
        return loadedView
    }
}
