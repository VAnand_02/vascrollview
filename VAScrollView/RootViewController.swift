//
//  RootViewController.swift
//  VAScrollView
//
//  Created by Vikash Anand on 25/05/19.
//  Copyright © 2019 Vikash Anand. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {

    //@IBOutlet var autolayoutScrollView: AutolayoutScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.autolayoutScrollView.delegate = self
        self.setup()
    }
    
    private func setup() {
        
        let autolayoutScrollView = AutolayoutScrollView(containerViewXibName: "View")//AutolayoutScrollView()//
        autolayoutScrollView.delegate = self
        self.view.add(autolayoutScrollView, safeAreaFlag: true)
    }
}


extension RootViewController: UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        print("Delegte called")
    }
}
