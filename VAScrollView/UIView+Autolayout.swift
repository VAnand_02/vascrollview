//
//  UIView+Autolayout.swift
//  VASymbolicateTest
//
//  Created by Vikash Anand on 25/05/19.
//  Copyright © 2019 Vikash Anand. All rights reserved.
//

import UIKit

@nonobjc extension UIView {
    
    func add(_ subView: UIView, withInsets insets: UIEdgeInsets = .zero, safeAreaFlag flag: Bool = false) {
        
        subView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(subView)
        
        let topAnchor = flag ? self.safeAreaLayoutGuide.topAnchor : self.topAnchor
        let leadingAnchor = flag ? self.safeAreaLayoutGuide.leadingAnchor : self.leadingAnchor
        let trailingAnchor = flag ? self.safeAreaLayoutGuide.trailingAnchor : self.trailingAnchor
        let bottomAnchor = flag ? self.safeAreaLayoutGuide.bottomAnchor : self.bottomAnchor
            
        NSLayoutConstraint.activate([
            subView.topAnchor.constraint(equalTo: topAnchor, constant: insets.top),
            subView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: insets.left),
            bottomAnchor.constraint(equalTo: subView.bottomAnchor, constant: insets.bottom),
            trailingAnchor.constraint(equalTo: subView.trailingAnchor, constant: insets.right)
        ])
    }
}
